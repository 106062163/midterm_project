var FormSection = document.getElementById("form-section");
var user_name = document.getElementById("user_name");
var currentUID;
var imgUrl;
var fileData
var up_img = document.getElementById("blah");
var postTitle = document.getElementById("new-post-title");
var postExplain = document.getElementById("form-explain");
var formSect = document.getElementById("form-section");
var electSection = document.getElementById("Electronics-show");
var bookSection = document.getElementById("Books-show");
var beautySection = document.getElementById("Beauty-show");
var foodSection = document.getElementById("Foods-show");
var otherSection = document.getElementById("Others-show");
var allSection = document.getElementById("All-show");
var listeningFirebaseRefs = [];
var sellForm = document.getElementById("productDataForm");
var allBut = document.getElementById("All-button");
var elecBut = document.getElementById("Electronics-button");
var bookBut = document.getElementById("Books-button");
var beautyBut = document.getElementById("Beauty-button");
var foodBut = document.getElementById("Foods-button");
var otherBut = document.getElementById("Others-button");
var signBut = document.getElementById("SignButton");
var formPrice = document.getElementById("sell-price");
var selectSort = document.getElementById("selectSort");
var inputImg = document.getElementById("upload_image");
var storageRef = firebase.storage().ref();
var productState = ["In shopping cart", "Waiting seller approval", "Transaction complete"]
var imageStore = []



function showSection(SectionEle, ButtonEle) {
  /* document.getElementById("form-section").style.display = "none"; */
  /* electSection.style.display = "none";
  bookSection.style.display = "none";
  beautySection.style.display = "none";
  foodSection.style.display = "none";
  otherSection.style.display = "none";
  allSection.style.display = "none";
  selectSort.style.display = "none"; */
  $("#form-section").hide("slow")
  $(electSection).hide("slow")
  $(bookSection).hide("slow")
  $(beautySection).hide("slow")
  $(foodSection).hide("slow")
  $(otherSection).hide("slow")
  $(allSection).hide("slow")
  $(selectSort).hide("slow")
  if (SectionEle) {
    $(SectionEle).show("slow")
    /* SectionEle.style.display = "block"; */
    selectSort.style.cssText = "display: flex !important";
  }
  if (ButtonEle) {
    ButtonEle.classList.add("is-active");
  }
}

firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // User is signed in.
    document.getElementById("user_div").style.display = "block";
    //document.getElementById("login_div").style.display = "none";
    document.getElementById("shop-section").style.display = "block";
    //document.getElementById("just-a-line").style.display = "block";
    $("#LogInOut").text("Log out");
    $("#SignButton").click(() => {
      firebase.auth().signOut().then(() => {
        document.location.reload(true);
      })
    });
    $("#cart-button").click(() => {
      queryShopCart()
      $("#shop-cart-modal").modal()
    })
    $("#BuySellButton").click(() => {
      queryBuyerProduct()
      ToSellForm();
    })
    currentUID = user.uid;
    var user = firebase.auth().currentUser;
    user_name.innerHTML = user.displayName;
    queryBuyerProduct(user.uid)
    querySellerProduct(user.uid)
    checkNotification(user.uid)

    $("#pills-home-tab").click(() => {
      queryBuyerProduct(user.uid)
    })
    $("#pills-profile-tab").click(() => {
      querySellerProduct(user.uid)
    })
  } else {
    // No user is signed in.
    $("#LogInOut").text("Log in");
    $("#SignButton").click(() => {
      $("#exampleModal").modal();
    });
    $("#cart-button").click(() => {
      BtAlert('Needing log in!');
    })
    $("#BuySellButton").click(() => {
      setTimeout(function () {
        BtAlert('Needing log in!');
      }, 0)
    })
  }
  starQueries();
});

function signUp() {
  var SignEmail = $("#signup-email")
  var signPw = $("#signup-password")
  var signPwRp = $("#signup-repeatPassword")
  var signName = $("#signup-name")
  if (SignEmail.val() != "" && signPw.val() != "" && signPwRp.val() != "" && signName.val() != "") {
    if (signPw.val() == signPwRp.val()) {
      firebase.auth().createUserWithEmailAndPassword(SignEmail.val(), signPw.val()).then(function (result) {
        var user = result.user
        writeUserData(user.uid, escapeHtml(signName.val()), escapeHtml(SignEmail.val()), "https://image.flaticon.com/icons/png/128/149/149071.png");
        alert("Sign up success!!")
        $("#LogIn-nav").click()
        $("input").val("")
      }).catch(function (error) {
        let errorM = error.message
        alert(errorM)
      });
    } else {
      alert("Error! password != Repeat password!!")
    }
  } else {
    alert("Error! someplace is empty!!")
  }
}

function login() {
  var userAccount = document.getElementById("login-email").value;
  var userPassword = document.getElementById("login-password").value;
  firebase.auth().signInWithEmailAndPassword(userAccount, userPassword).then(e => {
      document.location.reload(true);
    })
    .catch(function (error) {
      var errorMessage = error.message;
      window.alert("Error : " + errorMessage);
    });
}

function google_login() {
  var provider = new firebase.auth.GoogleAuthProvider();
  firebase
    .auth()
    .signInWithPopup(provider)
    .then((result) => {
      var user = result.user
      writeUserData(user.uid, user.displayName, user.email, user.photoURL);
      document.location.reload(true);
    });
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $("#blah")
        .attr("src", e.target.result)
        .width(450)
        .height(320);
      imgUrl = e.target.result;
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function ToSellForm() {
  var user = firebase.auth().currentUser;
  if (user) {
    showSection(null, null);
    if (document.getElementById("form-section").style.display == "none") {
      document.getElementById("form-section").style.display = "block";
    }
  } else {
    BtAlert("This feature need log in to unlocked!!")
  }

  return false;
}

function writeUserData(userId, name, email, imageUrl) {
  firebase.database().ref("users/" + userId).set({
    username: name,
    email: email,
    profile_picture: imageUrl
  });
}

function writeNewForm(title, text, username) {
  var selector = document.getElementById("select-section");
  var value = selector[selector.selectedIndex].value;
  var uid = firebase.auth().currentUser.uid;
  var SellPrice = $("#sell-price").val();

  var productData = {
    author: escapeHtml(username),
    type: escapeHtml(value),
    uid: escapeHtml(uid),
    body: escapeHtml(text),
    title: escapeHtml(title),
    buyCount: 0,
    price: SellPrice.padStart(9, "0")
  };
  var newPostKey = firebase.database().ref().child("products").push().key;
  var metadata = {
    contentType: 'image/jpeg'
  };
  var filename = newPostKey;
  var uploadTask = storageRef.child('images/' + filename).put(fileData, metadata)
  var updates = {};
  updates["/" + value + "/" + newPostKey] = productData;
  updates["/All/" + newPostKey] = productData;
  updates["/user-products/" + firebase.auth().currentUser.uid + "/" + newPostKey] = productData;
  firebase
    .database()
    .ref()
    .update(updates);
}

function newFormForCurrentUser(title, text, username) {
  var userId = firebase.auth().currentUser.uid;
  return firebase
    .database()
    .ref("/users/" + userId)
    .once("value")
    .then(function (snapshot) {
      var username = (snapshot.val() && snapshot.val().username) || "Anonymous";
      return writeNewForm(title, text, username);
    });
}

function queryBuyerProduct(buyer_uid) {
  var orderRef = firebase.database().ref('order/')
  var WaitBuySec = document.getElementById("buyer-check-wait")
  var DoneBuySec = document.getElementById("buyer-check-done")
  WaitBuySec.innerHTML = ""
  DoneBuySec.innerHTML = ""
  orderRef.on("child_added", (data) => {
    if (data.val().buyer_Uid == currentUID) {
      if (data.val().state == productState[1]) {
        var html =
          "<tr>" +
          "<td>" + data.val().title + "</td>" +
          "<td>" + data.val().quantity + "</td >" +
          "<td>" + data.val().totalPrice + "</td >" +
          "<td>" + data.val().state + "</td >" +
          "</tr>"
        var tr = document.createElement("tr");
        tr.innerHTML = html;
        WaitBuySec.appendChild(tr)
      } else if (data.val().state == productState[2]) {
        var html =
          "<tr>" +
          "<td>" + data.val().title + "</td>" +
          "<td>" + data.val().quantity + "</td >" +
          "<td>" + data.val().totalPrice + "</td >" +
          "<td>" + data.val().state + "</td >" +
          "</tr>"
        var tr = document.createElement("tr");
        tr.innerHTML = html;
        DoneBuySec.appendChild(tr)
      }
    }
  })
}

function querySellerProduct(seller_uid) {
  var sellerRef = firebase.database().ref('user-products/' + seller_uid)
  var proListPostition = document.getElementById("seller-check-list")
  proListPostition.innerHTML = ""
  sellerRef.orderByKey().on("child_added", (data) => {
    var html =
      '<tr>' +
      '<td>' + data.val().type + '</td>' +
      '<td>' + data.val().title + '</td>' +
      '<td>' +
      '<button class="" data-key="' + data.key + '"' +
      '>check order' +
      '</button>' +
      '</td >' +
      '<td>' +
      '<button class="btn btn-danger btn-xs" data-title="Delete">' +
      '<i class="material-icons">delete</i>' +
      '</button>' +
      '</td>' +
      '</tr>'
    var tr = document.createElement("tr");
    tr.innerHTML = html;
    var but = tr.getElementsByTagName("button")[0]
    var del = tr.getElementsByTagName("button")[1]
    $(but).click(() => {
      checkOrderModal(data.key)
    })
    $(del).click(() => {
      $("#dialog-confirm").dialog({
        modal: true,
        resizable: false,
        height: "auto",
        dialogClass: "test",
        width: 400,
        modal: true,
        responsive: true,
        create: function (event, ui) {
          $(this).css("z-index", "30")
        },
        buttons: {
          "Delete it now!": function () {
            firebase.database().ref('/' + data.val().type + '/' + data.key).remove()
            firebase.database().ref('/All/' + data.key).remove()
            firebase.database().ref('user-products/' + seller_uid + '/' + data.key).remove()
            $(tr).remove()
            $(this).dialog("close");
          },
          Cancel: function () {
            $(this).dialog("close");
          }
        }
      });
    })
    proListPostition.appendChild(tr)
  })
}


function checkOrderModal(key) {
  var orderRef = firebase.database().ref('order/')
  var tbody = document.getElementById("check-order-tbody")
  tbody.innerHTML = ""
  orderRef.orderByChild("product_Key").equalTo(key).on('child_added', (data) => {
    if (data.val().state == productState[1]) {
      var html =
        '<td >' +
        data.val().buyer_Name +
        '</td >' +
        '<td>' +
        data.val().quantity +
        '</td>' +
        '<td>' +
        '$' +
        data.val().totalPrice +
        '</td>' +
        '<td>' +
        '<input type="checkbox" class="ml-4" data-order=' + data.key + ' >' +
        '</td>'
      var trow = document.createElement('tr')
      trow.innerHTML = html
      tbody.appendChild(trow)
    }

  })
  $("#order-modal").modal()
}

function checkNotification(uid) {
  var orderRef = firebase.database().ref('order/')
  orderRef.orderByChild("seller_Uid").equalTo(uid).on('child_added', (data) => {
    if (data.val().state == productState[1]) {
      createNotify(data.val().title)
    }
  })
}

function starQueries() {
  /* var myUserId = firebase.auth().currentUser.uid; */
  var electRef = firebase.database().ref("Electronics");
  var bookRef = firebase.database().ref("Books");
  var beautyRef = firebase.database().ref("Beauty");
  var foodRef = firebase.database().ref("Foods");
  var otherRef = firebase.database().ref("Others");

  var allPostRef = firebase.database().ref("All");
  /* var allPostRef = firebase.database().ref("user-products/" + myUserId); */
  var fetchPosts = function (postsRef, sectionElement) {
    postsRef.orderByChild('buyCount').on("child_added", function (data) {
      var author = data.val().author || "Anonymous";
      var containerElement = sectionElement.getElementsByClassName("show_products")[0];
      containerElement.insertBefore(createPostElement(
          data.key,
          data.val().title,
          data.val().body,
          data.val().uid,
          data.val().authorPic,
          data.val().buyCount,
          data.val().type,
          data.val().price
        ),
        containerElement.firstChild);
    });
    $('#search-product').on('keyup', (e) => {
      if (e.keyCode == 13) {
        sectionElement.innerHTML = '<div class ="show_products">' + '</div>'
        if ($('#search-product').val() != "") {
          var searchWord = $('#search-product').val()
          postsRef.orderByChild("title").startAt(searchWord).endAt(searchWord + "\uf8ff").on("child_added", function (data) {
            var author = data.val().author || "Anonymous";
            var containerElement = sectionElement.getElementsByClassName("show_products")[0];
            containerElement.insertBefore(createPostElement(
                data.key,
                data.val().title,
                data.val().body,
                data.val().uid,
                data.val().authorPic,
                data.val().buyCount,
                data.val().type,
                data.val().price
              ),
              containerElement.firstChild);
          });
        } else {
          if ($("#sortBy").find(":selected").val() == 1) {
            postsRef.orderByChild('buyCount').on("child_added", function (data) {
              var author = data.val().author || "Anonymous";
              var containerElement = sectionElement.getElementsByClassName("show_products")[0];
              containerElement.insertBefore(createPostElement(
                  data.key,
                  data.val().title,
                  data.val().body,
                  data.val().uid,
                  data.val().authorPic,
                  data.val().buyCount,
                  data.val().type,
                  data.val().price
                ),
                containerElement.firstChild);
            });
          } else if ($("#sortBy").find(":selected").val() == 2) {
            postsRef.orderByChild('price').on("child_added", function (data) {
              var author = data.val().author || "Anonymous";
              var containerElement = sectionElement.getElementsByClassName("show_products")[0];
              containerElement.appendChild(createPostElement(
                  data.key,
                  data.val().title,
                  data.val().body,
                  data.val().uid,
                  data.val().authorPic,
                  data.val().buyCount,
                  data.val().type,
                  data.val().price
                ),
                containerElement.firstChild);
            });
          } else if ($("#sortBy").find(":selected").val() == 3) {
            postsRef.orderByChild('price').on("child_added", function (data) {
              var author = data.val().author || "Anonymous";
              var containerElement = sectionElement.getElementsByClassName("show_products")[0];

              containerElement.insertBefore(createPostElement(
                  data.key,
                  data.val().title,
                  data.val().body,
                  data.val().uid,
                  data.val().authorPic,
                  data.val().buyCount,
                  data.val().type,
                  data.val().price
                ),
                containerElement.firstChild);
            });
          }
        }
      }
    })
    $("#sortBy").change(() => {
      sectionElement.innerHTML = '<div class ="show_products">' + '</div>'
      if ($("#sortBy").find(":selected").val() == 1) {
        postsRef.orderByChild('buyCount').on("child_added", function (data) {
          var author = data.val().author || "Anonymous";
          var containerElement = sectionElement.getElementsByClassName("show_products")[0];
          containerElement.insertBefore(createPostElement(
              data.key,
              data.val().title,
              data.val().body,
              data.val().uid,
              data.val().authorPic,
              data.val().buyCount,
              data.val().type,
              data.val().price
            ),
            containerElement.firstChild);
        });
      } else if ($("#sortBy").find(":selected").val() == 2) {
        postsRef.orderByChild('price').on("child_added", function (data) {
          var author = data.val().author || "Anonymous";
          var containerElement = sectionElement.getElementsByClassName("show_products")[0];
          containerElement.appendChild(createPostElement(
              data.key,
              data.val().title,
              data.val().body,
              data.val().uid,
              data.val().authorPic,
              data.val().buyCount,
              data.val().type,
              data.val().price
            ),
            containerElement.firstChild);
        });
      } else if ($("#sortBy").find(":selected").val() == 3) {
        postsRef.orderByChild('price').on("child_added", function (data) {
          var author = data.val().author || "Anonymous";
          var containerElement = sectionElement.getElementsByClassName("show_products")[0];
          containerElement.insertBefore(createPostElement(
              data.key,
              data.val().title,
              data.val().body,
              data.val().uid,
              data.val().authorPic,
              data.val().buyCount,
              data.val().type,
              data.val().price
            ),
            containerElement.firstChild);
        });
      }
    })
    postsRef.on("child_changed", function (data) {
      var containerElement = sectionElement.getElementsByClassName("show_products")[0];
      var changeElement = containerElement.getElementsByClassName(data.key + "-div")[0];
      changeElement.getElementsByClassName("card-title")[0].innerHTML = "Title:" + data.val().title;
      changeElement.getElementsByClassName("card-price")[0].innerText = parseInt(data.val().price, 10);
      changeElement.getElementsByClassName("card-text")[0].innerText = data.val().body;
    });
    postsRef.on("child_removed", function (data) {
      var containerElement = sectionElement.getElementsByClassName("show_products")[0];
      var RemoveElement = containerElement.getElementsByClassName(data.key + "-div")[0];
      RemoveElement.parentElement.removeChild(RemoveElement);
    });


  };
  fetchPosts(electRef, electSection);
  fetchPosts(bookRef, bookSection);
  fetchPosts(beautyRef, beautySection);
  fetchPosts(foodRef, foodSection);
  fetchPosts(otherRef, otherSection);
  fetchPosts(allPostRef, allSection);

  /*  listeningFirebaseRefs.push(electRef);
   listeningFirebaseRefs.push(bookRef);
   listeningFirebaseRefs.push(beautyRef);
   listeningFirebaseRefs.push(foodRef);
   listeningFirebaseRefs.push(otherRef);
   listeningFirebaseRefs.push(allPostRef); */
}

function createPostElement(
  key,
  title,
  body,
  uid,
  authpic,
  buyCount,
  type,
  price
) {
  /* storageRef.child('images/' + key).getDownloadURL().then(url => {
    var allimg = document.querySelectorAll("#" + key)
    allimg.forEach((e) => {
      if ($(e).is("img")) e.src = url
      imageStore.key = url;
    })
  }) */
  var html =
    '<div class="container d-flex justify-content-around mb-4">' +
    '<div class="show-container ' + key + "-div" + '">' +
    '<img src="#" data-type="product"' +
    'id = "' + key + '"' +
    'class="post-pic" alt = "Fail to show image" > ' +
    '<span class="card-body post-body">' +
    '<h5 class="card-title">Title:' +
    title +
    "</h5>" +
    "<h5>Price:$" +
    '<span class="card-price">' +
    parseInt(price, 10) +
    '</span>' +
    "</h5>" +
    '<p class="card-text">' +
    body +
    '</p>' +
    '<i class="material-icons" data-toggle="tooltip" data-placement="bottom" title="Add to cart">' +
    'add_shopping_cart</i> ' +
    "</span>" +
    "</div>" +
    "</div>";
  var div = document.createElement("div");
  div.innerHTML = html;

  $(div).find("i").click(() => {
    var user = firebase.auth().currentUser
    if (user) {
      firebase.database().ref("/order/").once('value', function (snapshot) {
        var exist = false
        snapshot.forEach((child) => {
          if (child.val().product_Key == key && child.val().state == productState[0]) exist = true
        })
        if (exist == false) {
          addToShopCart(key, uid, currentUID, title, price)
        } else
          BtAlert("already existed in shopping cart!!")
      });
    } else {
      setTimeout(BtAlert("Log in please!!"), 0)
    }
  })
  var postElement = div.firstChild;

  return postElement;
}



function queryShopCart() {
  var tbody = document.getElementById('cart-tbody')
  tbody.innerHTML = ""
  var orderRef = firebase.database().ref('order/');
  orderRef.once('value', (snapshot) => {
    snapshot.forEach((data) => {
      if (data.val().buyer_Uid == currentUID && data.val().state == productState[0]) {
        /* var html =
          '<div id="' + data.key + '" >' +
          '<td style = "width:100px;" >' +
          '<img class = "img img-fluid" src = "' + imageStore.key + '" style = "width:100px;height:auto;" alt = "some error" >' +
          '</td>' +
          '<td class="text-truncate align-middle" style = "max-width:200px;" >' +
          '<p class = "text-truncate" > ' + data.val().title + ' </p> ' +
          '</td>' +
          '<td class="float-right mr-4" width="110" >$' +
          '<span >' +
          parseInt(data.val().price, 10) +
          '</span>' +
          ' </td>' +
          '<td class = "float-right mr-4" >' +
          '<input class = "text-center" type = "number" style = "width:60px;" value = "1" min = "0" ' +
          '</td>' +
          '<td class = "" >' +
          '<button class = "btn btn-danger btn-sm" id="' + '-removeBut"> <i class = "material-icons" > cancel</i> </button >' +
          '</td>' +
          '</div>' */

        var Imgkey = imageStore[data.val().product_Key]
        var html =
          '<td width="130px">' +
          '<img class = "img img-fluid" src = "' + Imgkey + '" style = "width:100px;height:auto;" alt = "some error" >' +
          '</td>' +
          '<td class="text-truncate align-middle" style = "max-width:200px;" >' +
          '<p class = "text-truncate" > ' + data.val().title + ' </p> ' +
          '</td>' +
          '<td class="align-middle">' +
          '<input class = "text-center align-middle" type = "number" style = "width:60px;" value = "1" min = "0" ' +
          '</td>' +
          '<td class="align-middle" width="110" >$' +
          '<span >' +
          parseInt(data.val().price, 10) +
          '</span>' +
          ' </td>' +
          '<td class="align-middle" >' +
          '<button class = "btn btn-danger btn-sm" id="' + '-removeBut"> <i class = "material-icons" > cancel</i> </button >' +
          '</td>'

        var trow = document.createElement('tr')
        trow.id = data.val().product_Key + '-cart-field'
        trow.dataset.orderkey = data.key
        trow.innerHTML = html;
        tbody.appendChild(trow)
        trow.getElementsByTagName("input")[0].addEventListener('keyup', calculatePrice)
        $('#' + data.val().product_Key + '-cart-field').find("button").click(function () {
          $('#' + data.val().product_Key + '-cart-field').remove()
          if (tbody.innerHTML == "") {
            tbody.innerHTML = '<p >there are nothing stuff in your cart!</p>';
          }
          firebase.database().ref('order/' + data.key).remove()
        })
      }
    })
    if (tbody.innerHTML == "") {
      tbody.innerHTML = '<p >there are nothing stuff in your cart!</p>';
    }
  })

}

async function addToShopCart(key, sellerUid, buyerUid, title, price) {
  if (sellerUid === buyerUid) {
    setTimeout(BtAlert("this is your stuff!!"))
    return;
  }
  firebase.database().ref('/users').once('value').then(snapshot => {
    var sellerName = snapshot.val()[sellerUid].username
    var buyerName = snapshot.val()[buyerUid].username
    var updateData = {
      title: title,
      price: price,
      product_Key: key,
      buyer_Uid: buyerUid,
      buyer_Name: buyerName,
      seller_Uid: sellerUid,
      seller_Name: sellerName,
      state: productState[0]
    }
    firebase.database().ref("/order/").push(updateData)
  })
}

function calculatePrice() {
  var plb = document.getElementById("price-label")
  var allList = document.querySelectorAll("#cart-tbody tr")
  var result = 0
  allList.forEach((snapshot) => {
    if (snapshot) {
      var quantity = snapshot.getElementsByTagName("input")[0]
      var price = snapshot.getElementsByTagName("span")[0]
      result += eval(quantity.value * price.innerHTML)
    }
  })
  plb.innerHTML = result
}

function addBuyCount(key, n) {
  var Allref = firebase.database().ref('/All/' + key + '/buyCount')
  firebase.database().ref('/All/' + key).once("value", (data) => {
    var thistype = data.val().type
    var thisref = firebase.database().ref('/' + thistype + '/' + key + '/buyCount')
    thisref.transaction((a) => {
      return a + 1
    })
  })
  Allref.transaction((e) => {
    return e + 1
  })


}

window.addEventListener("load", function () {
    formSect.onsubmit = function (e) {
      e.preventDefault();
      var text = postExplain.value;
      var title = postTitle.value;
      var prices = $("#sell-price").val();
      if (text && title && prices) {
        newFormForCurrentUser(title, text).then(function () {
          //myPostsMenuButton.click();
          postExplain.value = "";
          postTitle.value = "";
          $("#sell-price").val("");
          up_img.src = "#";
        });
        setTimeout(alert("Add product successfully!"), 0)
      } else {
        setTimeout(alert("missing something!!"), 0)
      }
    };
    allBut.onclick = function () {
      showSection(allSection, allBut);
    };
    elecBut.onclick = function () {
      showSection(electSection, elecBut);
    };
    bookBut.onclick = function () {
      showSection(bookSection, bookBut);
    };
    beautyBut.onclick = function () {
      showSection(beautySection, beautyBut);
    };
    foodBut.onclick = function () {
      showSection(foodSection, foodBut);
    };
    otherBut.onclick = function () {
      showSection(otherSection, otherBut);
    };

    inputImg.addEventListener('change', (e) => {
      fileData = e.target.files[0]
    })
    allBut.click()
    var prices = document.getElementById("sell-price")
    prices.oninput = (e) => {
      if (prices.value.length > 8) prices.value = prices.value.slice(0, 8);
    }
    prices.onkeypress = (e) => {
      if (e.keyCode == 45) {}
      event.preventDefault();
      return false;
    }

    $("#modal-signup-but").on('click', () => signUp())
    $("#modal-signin-but").on('click', () => login())
    $("#google-but").on('click', () => {
      google_login()
    })
    $('#signup-password,#signup-repeatPassword').on('keyup', function () {
      if ($('#signup-password').val() != "" && $('#signup-repeatPassword').val() != "") {
        if ($('#signup-password').val() == $('#signup-repeatPassword').val()) {
          $("#lock_1").html("lock_open")
          $("#lock_2").html("lock_open")
          $('#signup-password').removeClass("border border-danger")
          $('#signup-repeatPassword').removeClass("border border-danger")
          $('#signup-password').addClass("border border-success")
          $('#signup-repeatPassword').addClass("border border-success")
        } else {
          $("#lock_1").html("lock")
          $("#lock_2").html("lock")
          $('#signup-password').removeClass("border border-success")
          $('#signup-repeatPassword').removeClass("border border-success")
          $('#signup-password').addClass("border border-danger")
          $('#signup-repeatPassword').addClass("border border-danger")
        }
      } else {
        $('#signup-password').removeClass("border border-danger")
        $('#signup-repeatPassword').removeClass("border border-danger")
        $('#signup-password').removeClass("border border-success")
        $('#signup-repeatPassword').removeClass("border border-success")
      }
    });
    var numInp = document.querySelectorAll("input")
    numInp.forEach((e) => {
      if (e.type == "number") {
        e.onkeypress = (a) => {
          if (a.keyCode == 45 || e.value == "0") {
            event.preventDefault()
            return false;
          }
        }
      }
    })
    $('.modal').on('shown.bs.modal', function () {
      var margin_vertical = parseInt($(this).find('.modal-dialog').css('margin-top')) + parseInt($(this).find('.modal-dialog').css('margin-bottom')) || 0;
      var height_header = parseInt($(this).find('.modal-header').css('height')) || 0;
      var height_footer = parseInt($(this).find('.modal-footer').css('height')) || 0;
      var height_body = (window.innerHeight - height_header - height_footer - margin_vertical - 10) + 'px';
      $(this).find('.modal-body').css('max-height', height_body).css('overflow', 'auto');
    });
    var cartTbody = document.getElementById("cart-tbody")
    cartTbody.addEventListener('DOMSubtreeModified', () => {
      calculatePrice()
    })
    $("#submit-order-but").click(() => {
      var allList = document.querySelectorAll("#cart-tbody tr")
      allList.forEach((snapshot) => {
        var orderKey = snapshot.dataset.orderkey
        var orderKeyRef = firebase.database().ref('order/' + orderKey)
        var price = snapshot.getElementsByTagName("span")[0].innerHTML
        var quantity = snapshot.getElementsByTagName("input")[0].value
        if (quantity != "" && quantity != 0) {
          orderKeyRef.update({
            state: productState[1],
            quantity: quantity,
            totalPrice: eval(price * quantity)
          })
        }
      })
      alert("Success to sumbit order!!")
      queryShopCart()
      $("#shop-cart-modal").modal('hide')
    })
    var svgBox = document.getElementById("svgBox")
    svgBox.onclick = () => {
      document.location.reload(true);
    }
    $("#confirm-all").click(() => {
      var tbody = document.getElementById("check-order-tbody")
      var chBoxs = tbody.querySelectorAll("input")
      chBoxs.forEach((data) => {
        data.checked = true;
      })
    })
    $("#confirm-order-but").click(() => {
      var tbody = document.getElementById("check-order-tbody")
      var chBoxs = tbody.querySelectorAll("input")
      chBoxs.forEach((data) => {
        if (data.checked == true) {
          var orderRef = firebase.database().ref('order/' + data.dataset.order)
          orderRef.update({
            state: productState[2]
          })
          orderRef.once("value", (snapshot) => {
            addBuyCount(snapshot.val().product_Key, snapshot.val().quantity)
          })
        }
      })
      alert("Confirm success!!")
      $("#order-modal").modal('hide')
    })
    setInterval(() => {
      var allimage = document.querySelectorAll("img")
      allimage.forEach((img) => {
        if (img.dataset.type == "product") {
          var imgId = img.id
          if (!imageStore[imgId]) {
            storageRef.child('images/' + imgId).getDownloadURL().then(onResolve, onReject)
          }

          function onResolve(url) {
            imageStore[imgId] = url
          }

          function onReject(e) {
            console.log(e.message)
          }
          if (imageStore[imgId]) img.src = imageStore[imgId]
        }
      })
    }, 1000);
  },
  false
);

function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function (m) {
    return map[m];
  });
}
window.addEventListener('load', function () {
  Notification.requestPermission(function (status) {
    // This allows to use Notification.permission with Chrome/Safari
    if (Notification.permission !== status) {
      Notification.permission = status;
    }
  });
});



function createNotify(title) {
  var notifyConfig = {
    body: "Someone bought your stuff!!\n" + title,
    icon: "./image/notice-icon.png"
  }
  if (!("Notification" in window)) {
    console.log("This browser does not support notification");
  } else if (Notification.permission === "granted") {
    var notification = new Notification(
      "New message", notifyConfig
    );
  } else if (Notification.permission !== "denied") {
    Notification.requestPermission(function (permission) {
      if (permission === "granted") {
        var notification = new Notification("Hi there!", notifyConfig);
      }
    });
  }
}

function BtAlert(S) {
  var html =
    '<div class = "alert alert-danger alert-dismissible" role="alert">' +
    '<strong>Error!</strong>' + S +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close" >' +
    '<span aria-hidden = "true">&times; </span>' +
    '</button>' +
    '</div>'
  $("#alert-field").html(html)
}