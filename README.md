# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Hi buy shopping 
* Key functions (add/delete)
    1. Shopping web page
    
* Other functions (add/delete)
    1. Sign in/up , google sign-in
    2. product page
    3. shopping pipeline
    4. user dashboard

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# [作品網址](https://my-first-database-b3157.firebaseapp.com/)：https://my-first-database-b3157.firebaseapp.com/

# Components Description : 
# <h1>1. Sign in/up, google sign-in (Membership Mechanism):</h1> 

### First using a gif to show the process

<img src="/image/bg0j0-cp6fo.gif">

### Sign in: can using google account, or use a new account by register.</h3>
### Sign up: input detail, and should fulfill all input box</h3>
### Sign up error happen when: 
* <h4>mail wrong format/already exist.</h4>
* <h4>password and repeat-password are different.</h4>
* <h4>didn't fulfill all input boxes.

# <h1>2. Database </h1>
* <h3>Add product to sell</h3>

### After log in (this feature just for user)
### first click the Buying/Selling button on upper right corner
### Form will appear after clicking "Add a product for selling" button
<img src="./image/Sellform.PNG">

### Fill in the product information, then press submit button.

<img src="./image/fillForm.PNG" width="500px" height="auto">

### then there will exist a product in database and show in product page
* <h3>read data(product) in product page:</h3>

<img src="./image/readData.PNG" width="600px" height="auto">

# <h1>3. RWD </h1>
### Ensure the page would not explode in different size of window
<img src="./image/smallSize.PNG">

# <h1>4. Topic key function</h1>
* <h3>Product page</h3>
<img src="./image/kxv8p-uvp81.gif" width="900px" height="600px">

* <h3>Shopping pipeline and user dashboard </h3>
<img src="./image/xtgf8-5l0r3.gif" width="900px" height="600px">

* <h3>Seller delete product </h3>
<img src="./image/vqxfj-f9y9n.gif" width="900px" height="600px">

# Advanced Components:
* ### Third-Party Sign In:
<img src="./image/3pSignIn.PNG">

* ### Chrome Notification:
<h4>For reminding seller their products were bought</h4>
<img src="./image/notif.PNG">

* ### Use CSS Animation:
<p><h4>A welcome head title on upper-left corner</h4></p>
<img src="./image/9p2u2-pendj.gif">


# Other Functions Description(1~10%) : 
### 1. Searching product
* <h4>user can search product by 商品的開始字元</h4>
<h4>searching Kamen, press enter button, and appear the product:</h4>
<img src="./image/searchKamen.PNG">

### 2. Sort product by price
<h4> In firebase, number is sort by string way</h4>
<h4> to avoid wrong sorting, i have to convert the number to something special in database like that</h4>
<img src="./image/priceIndatabase.PNG">

### then the sorting works properly.

...

## Security Report (Optional)
<h4>1. All data that will be written to the database is only available via user.</h4>
<h4>2. Most feature required log in to use except view product.</h4>
<h4>3. I have used a function to replace some special characters with other words</h4>
<img src="./image/Sec.PNG">
